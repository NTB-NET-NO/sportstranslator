using System;
using System.IO;
using System.Collections;
using System.Management;
using System.Diagnostics;
using ntb_FuncLib;



namespace SportsTranslator
{
	/// <summary>
	/// This class deals with everything that has to do with IO.
	/// For instance creating directories, loading files, saving files and so on
	/// </summary>
	public class io
	{
        private EventLog evt = new EventLog();
        private string Sourcename = "SportsTranslator";
        private static LogFile logFile = new LogFile();
        
        private static string dirInput = Properties.Settings.Default.DirectoryInput;
        private static string dirOutput = Properties.Settings.Default.DirectoryOutput;
        private static string dirDictionary = Properties.Settings.Default.DirectoryDictionary;
        private static string dirLog = Properties.Settings.Default.DirectoryLog;
        private static string dirError = Properties.Settings.Default.DirectoryError;
        private static string dirDone = Properties.Settings.Default.DirectoryDone;

		public io()
		{
			//
			// TODO: Add constructor logic here
			//

            string message = "Creating IO object";
            if (!Directory.Exists(dirLog))
            {
                if (!EventLog.SourceExists(Sourcename))
                {
                    EventLog.CreateEventSource(Sourcename, "SportsTranslator");
                }

                // EventLog.Source = "SportsTranslator";
                evt.Log = Sourcename;
                evt.Source = Sourcename;

                evt.WriteEntry(message, EventLogEntryType.Information);

            } else {
                LogFile.WriteLog(ref dirLog, ref message);
            }
		}

		/// <summary>
		/// This method reads the content of the file
		/// </summary>
		/// <param name="Filename">String containing the name of the file to be read</param>
		/// <returns>Content of the file</returns>
		public string readFile (string Filename) 
		{
			if (!File.Exists(Filename) )
			{
				throw (new FileNotFoundException(
					Filename + " cannot be read since it does not exist.", Filename));
			}

			string contents = "";

			using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
					   FileAccess.Read, FileShare.None)) 
			{
				using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.Default)) 
				{
					contents = streamReader.ReadToEnd();
				}
			}

			return contents;


		}

		/// <summary>
		/// This method writes the file to the directory
		/// </summary>
		/// <param name="Filename">String containing the filename</param>
		/// <param name="data">String containing the data that shall be written to the file</param>
		public void writeFile (string Filename, string data) 
		{
            try
            {
                FileInfo newFile = new FileInfo(Filename);
                if (newFile.Exists == true)
                {
                    newFile.Delete();
                }

                using (FileStream fileStream = new FileStream(Filename, FileMode.CreateNew,
                           FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter streamWriter = new StreamWriter(fileStream))
                    {
                        streamWriter.WriteLine(data);
                    }
                }
            }
            catch (IOException iox)
            {
                string message = iox.ToString();
                LogFile.WriteLog(ref dirLog, ref message);
            }
            catch (Exception e)
            {
                string message = e.ToString();
                LogFile.WriteLog(ref dirLog, ref message);
            }
		}

		/// <summary>
		/// This method checks if the file exists or not
		/// </summary>
		/// <param name="Filename">Filename we are to check existance of</param>
		/// <returns>Returns false if the file exists, otherwize true</returns>
		public bool FileExists (string Filename) 
		{
			if (File.Exists(Filename)) 
			{
				return true;
			} 
			return false;
		}

		/// <summary>
		/// This method reads the content of a file into an array/collection
		/// </summary>
		/// <param name="Filename">name of the file we are to load</param>
		/// <returns>Returns the content of the file in an array</returns>
		public Hashtable ReadFileToArray (string Filename) 
		{
            // Creating a string
            string strContents = "";

            // Creating the dictionary hashtable (array)
            Hashtable dictionary = new Hashtable();

            try
            {
                string message = "Creating IO object";
                if (!Directory.Exists(dirLog))
                {
                    evt.WriteEntry(message, EventLogEntryType.Information);
                }
                else
                {
                    LogFile.WriteLog(ref dirLog, ref message);
                }

                if (!File.Exists(Filename))
                {
                    evt.WriteEntry("File " + Filename + " does not exists!", EventLogEntryType.Information);
                    throw (new FileNotFoundException(
                        Filename + " cannot be read since it does not exist.", Filename));
                    
                }


                using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
                           FileAccess.Read, FileShare.None))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.Default))
                    {
                        while (streamReader.Peek() != -1)
                        {
                            /*
                             * We know that the format of the line is like this:
                             * Word1:ReplaceWord1
                             */
                            strContents = streamReader.ReadLine();
                            string[] strWords = strContents.Split(':');

                            int Count = strWords.Length;
                            message = "The number of items in array: " + Count.ToString();
                            if (!Directory.Exists(dirLog))
                            {
                                evt.WriteEntry(message,
                                    EventLogEntryType.Information);
                            }
                            else
                            {
                                LogFile.WriteLog(ref dirLog, ref message);
                            }

                            if (!dictionary.Contains(strWords[0]))
                            {

                                dictionary.Add(strWords[0], strWords[1]);
                            }



                        }
                    }
                }

                return dictionary;
            }
            catch (IOException ioe)
            {
                evt.WriteEntry(ioe.ToString(),
                    EventLogEntryType.Error);
                return dictionary;
            }
            catch (Exception e)
            {
                evt.WriteEntry(e.ToString(),
                    EventLogEntryType.Error);
                return dictionary;
            }
            

		}

		/// <summary>
		/// This method creates a directory if it does not exists
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void createDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;
			if (!Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);
				dirInfo.Create();
			}
		}

		/// <summary>
		/// This method checks if the directory exists or not
		/// </summary>
		/// <param name="DirectoryName">Name of directory to check - complete path</param>
		/// <returns>Returns false if the directory does not exists, otherwize true</returns>
		public bool getDirectory (string DirectoryName) 
		{
			if (!Directory.Exists(@DirectoryName)) 
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// This method deletes the directory and subdirectories within
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void deleteDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;

			if (Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);

				dirInfo.Delete(true);
			}
		}

        /// <summary>
        /// This method shall move a file from one directory to another
        /// </summary>
        /// <param name="FromDirectory">This string holds the path for the from directory</param>
        /// <param name="ToDirectory">This string holds the path for the to directory</param>
        public void MoveFile(string FromDirectory, string ToDirectory, string fileName)
        {
            FileInfo fileInfo = null;

            if (Directory.Exists(FromDirectory))
            {
                if (Directory.Exists(ToDirectory))
                {
                    fileInfo.MoveTo(ToDirectory + @"\" + fileName);
                }
            }
        }

        /// <summary>
        /// This method is a overload of the prior method and moves the file
        /// </summary>
        /// <param name="ToDirectory">This string holds the name of the directory to move the file</param>
        /// <param name="fileName">This string holds the name of the file we are to move</param>
        public void MoveFile(string OldFilename, string NewFilename)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(OldFilename);
                FileInfo newFile = new FileInfo(NewFilename);

                if (newFile.Exists == true)
                {
                    newFile.Delete();
                }
                fileInfo.MoveTo(NewFilename);
            }
            catch (IOException ioex)
            {
                string message = ioex.ToString();
                LogFile.WriteLog(ref dirLog, ref message);
            }
            catch (Exception ex)
            {
                string message = ex.ToString();
                LogFile.WriteLog(ref dirLog, ref message);
            }
        }

	}
}
