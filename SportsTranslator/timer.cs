﻿using System;
using System.Timers;
using System.Diagnostics;
using ntb_FuncLib;

namespace SportsTranslator
{
    public class timer
    {
        // private static System.Timers.Timer aTimer;

        private static EventLog evt = new EventLog();
        private static LogFile logFile = new LogFile();
        private static TranslatorService ts = new TranslatorService();

        private static string dirInput = Properties.Settings.Default.DirectoryInput;
        private static string dirOutput = Properties.Settings.Default.DirectoryOutput;
        private static string dirDictionary = Properties.Settings.Default.DirectoryDictionary;
        private static string dirLog = Properties.Settings.Default.DirectoryLog;
        private static string dirError = Properties.Settings.Default.DirectoryError;
        private static string dirDone = Properties.Settings.Default.DirectoryDone;
        
        private static string Sourcename = "SportsTranslator";

        public static void Main ()
        {
            if (!EventLog.SourceExists(Sourcename))
            {
                EventLog.CreateEventSource(Sourcename, "SportsTranslator");
            }

                       
            // EventLog.Source = "SportsTranslator";
            //evt.Source = Sourcename;
            //evt.WriteEntry("In Timer.Main",
            //    EventLogEntryType.Information);
            string message = "In Timer.Main";
            LogFile.WriteLog(ref dirLog, ref message);

            // Create a timer with a ten second interval
            System.Timers.Timer aTimer = new System.Timers.Timer();

            // Hook up the Elapsed event for the timer
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);



            // Set the timer interval to two minutes (120000 milliseconds)
            // aTimer.Interval = Properties.Settings.Default.TimerInterval;

            // During test (debug) we are setting this to 2000 milliseconds - 2sec
            aTimer.Interval = 2000;

            // Enable Timer
            aTimer.Enabled = true;

            // Keep the timer alive until the end of Main.
            GC.KeepAlive(aTimer);

        }


        

        public static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            /*
            evt.WriteEntry("In timer-class OnTimedEvent", 
                EventLogEntryType.Information);

            evt.WriteEntry("Creating the translator object",
                EventLogEntryType.Information);
            translator trans = new translator();

            // We are running the translator and translating files
            evt.WriteEntry("Translating files",
                EventLogEntryType.Information);
            trans.TranslateFiles();
             */
            string strEvent = "The Elapsed event was raised at " + e.SignalTime;
            //evt.WriteEntry(strEvent, 
            //    EventLogEntryType.Information);

            LogFile.WriteLog(ref dirLog, ref strEvent);
            

        }
    }
}
