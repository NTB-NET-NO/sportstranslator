using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Management;
using System.Globalization;
using ntb_FuncLib;



namespace SportsTranslator
{
	/// <summary>
	/// Summary description for translator.
	/// </summary>
	public class translator
	{

        // This holds the dictionary
		public Hashtable arrDictionary = new Hashtable();

        // Creating the logfile
        private static LogFile logFile = new LogFile();
        private static EventLog evt = new EventLog();

        private static TranslatorService ts = new TranslatorService();
        
        private static string dirInput = Properties.Settings.Default.DirectoryInput;
        private static string dirOutput = Properties.Settings.Default.DirectoryOutput;
        private static string dirDictionary = Properties.Settings.Default.DirectoryDictionary;
        private static string dirLog = Properties.Settings.Default.DirectoryLog;
        private static string dirError = Properties.Settings.Default.DirectoryError;
        private static string dirDone = Properties.Settings.Default.DirectoryDone;
        private string message = "";
        
        private string Sourcename = "SportsTranslator";

        /// <summary>
        /// This method shall be called when we start up the service
        /// </summary>
        public void Start()
        {
            /*
             * We shall check if the directories in the settings-file exists
             * If they don't, we shall create them.
             */

            // creating EventLog object
            // This should read the text-file - if it has changed - then do search and replace
            // This string should get it's name from the ProjectInstaller - attributes

            
            // string Sourcename = this.ServiceName;



            
            


        }

        public void temp_shall_be_renamed()
        {
            
            // Then we shall start the rest of the stuff, looping through the
            // Dictionary and so on

            // The code below shall not be used at this moment


            // this code shall be changed so that it loops a directory and gets the files that's in it.
            // This is hardcoded just to be sure that it works. We can add
            

            /*
             * The code below shall be changed to be put into a string
             * and then be saved to the a file in output directory
             */

            // string String = st.translate(content);
        }

        public void TranslateFiles () {
            // Process the list of files found in the directory.
            try
            {

                message = "Creating IO-object";


                LogFile.WriteLog(ref dirLog, ref message);

                // Creating the IO-object
                io IO = new io();

                // Telling where we are in the world
                message = "TranslatorService Object";
                LogFile.WriteLog(ref dirLog, ref message);
                //            evt.WriteEntry(message,
                //                EventLogEntryType.Information);

                message = "This is dirInput: " + dirInput + "\n";
                message += "This is dirOutput: " + dirOutput + "\n";

                evt.WriteEntry(message,
                    EventLogEntryType.Information);

                LogFile.WriteLog(ref dirLog, ref message);


                if (dirInput != "")
                {
                    string[] fileEntries = Directory.GetFiles(dirInput);


                    foreach (string fileName in fileEntries)
                    {
                        // do something with fileName
                        message = fileName + " is loaded and will be translated";
                        LogFile.WriteLog(ref dirLog, ref message);
                        string content = IO.readFile(fileName);

                        // Translating file
                        content = this.translate(content);

                        // Write to new file - which is in Output directory
                        // evt.WriteEntry(fileName + " is translated and will be saved");
                        message = fileName + " is translated and will be saved";
                        LogFile.WriteLog(ref dirLog, ref message);

                        // We are to check if \ is in the filename
                        // We pretty much know that it is, so I split it and use the last one
                        
                        string[] arrFilename = fileName.Split((@"\").ToCharArray());


                        string saveFile = dirOutput + @"\" + arrFilename[arrFilename.Length-1];
                        string doneFile = dirDone + @"\" + arrFilename[arrFilename.Length - 1];

                        // evt.WriteEntry(saveFile + " is the new name");
                        message = saveFile + " is the new name\n";
                        message += doneFile + " is where we put the original file";

                        LogFile.WriteLog(ref dirLog, ref message);
                        // IO.writeFile(fileName, content);

                        // And we shall move the translated file to the done-directory
                        // IO.moveFile(fileName);
                        // Writing the new file
                        IO.writeFile(saveFile, content);
                        
                        // Moving the done file
                        IO.MoveFile(fileName, doneFile);

                        // We shall 

                    }
                }
                else
                {
                    message = "dirInput is empty: " + dirInput;
                    LogFile.WriteLog(ref dirLog, ref message);
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();

                if (dirLog != "")
                {
                    LogFile.WriteLog(ref dirLog, ref message);
                }
                else
                {
                    evt.WriteEntry(message,
                        EventLogEntryType.Error);
                }
                
            }


            

            
        }

        /// <summary>
        /// This method reads the dictionary file and returns the file as an array
        /// </summary>
        public void readDictionary()
        {
            // TODO: We shall create a flag/file which says when the dictionary-file was read
            message = "In readDictionary method: Reading Dictionary file";
            LogFile.WriteLog(ref dirLog, ref message);
            // evt.WriteEntry(message, EventLogEntryType.Information);
            io IO = new io();

            // I don't know why we do this...
            io fileDir = new SportsTranslator.io();
            TranslatorService ts = new TranslatorService();

            string DicFilename = dirDictionary + "/" + Properties.Settings.Default.FileDictionary;
            
            message = "In readDictionary method: Checking if file exists. If not - Create one";
            LogFile.WriteLog(ref dirLog, ref message);
            
            // evt.WriteEntry(message, EventLogEntryType.Information);
            if (fileDir.FileExists(DicFilename) == false)
            {
                message = "Dictionary file does not exists. Creating one!";
                LogFile.WriteLog(ref dirLog, ref message);
                // evt.WriteEntry(message, EventLogEntryType.Information);
                // We create the file empty
                fileDir.writeFile(DicFilename, "");
            }

            message = "In readDictionary method: Adding file content to array";
            LogFile.WriteLog(ref dirLog, ref message);
            // evt.WriteEntry(message, EventLogEntryType.Information);
            arrDictionary = fileDir.ReadFileToArray(DicFilename);

            // return arrDictionary;
            message = "In readDictionary method: Done Dictionary file";
            // evt.WriteEntry(message, EventLogEntryType.Information);
            LogFile.WriteLog(ref dirLog, ref message);
        }
		

        public void Main () {
            // string dirLog = dirLog;
        }

        /// <summary>
        /// The constructor for this class. Doesn't do much really.
        /// But we can make it read the dictionary file, which means it will be 
        /// read every two minutes... Do we want that?
        /// </summary>
		public translator()
		{

            if (dirLog != "")
            {
                LogFile.MakePath(dirLog);
            }

            if (!EventLog.SourceExists(Sourcename))
            {
                EventLog.CreateEventSource(Sourcename, "SportsTranslator");
            }

            evt.Log = Sourcename;
            evt.Source = Sourcename;


            //evt.WriteEntry("Entering Start in translator class", EventLogEntryType.Information);

            // Then we shall read the dictionary-file
            string message = "Reading dictionary file";
            LogFile.WriteLog(ref dirLog, ref message);
            //evt.WriteEntry("Reading dictionary file", EventLogEntryType.Information);
            readDictionary();
			

		}

		/// <summary>
		/// This method shall find the word and - if it is in the dictionary-file
		/// we shall replace the name with other parameters
		/// </summary>
		/// <param name="content">String containing the file we are replacing words in</param>
		public string translate(string content) 
		{
            // TODO: We should check if the dictionaryfile is read and how old the file is
            // TODO: If the file is updated we shall read it again
            message = "In translator.translate starting to translate file";
            LogFile.WriteLog(ref dirLog, ref message);
            //evt.WriteEntry(message,
            //        EventLogEntryType.Warning);

            // Adding a check to see if arrDictionary holds any data
            if (arrDictionary.Count == 0)
            {
                this.readDictionary();
            }


            // Loops through the items in the array
			foreach (DictionaryEntry Item in arrDictionary ) 
			{
                // Doing the replace
                message = "Replacing items in the file";
                LogFile.WriteLog(ref dirLog, ref message);
                //evt.WriteEntry(message,
                    //EventLogEntryType.Information);
                content = content.Replace(Item.Key.ToString(), Item.Value.ToString());
				
			}
            LogFile.WriteLog(ref dirLog, ref content);
			return content;
		}

	}
}
