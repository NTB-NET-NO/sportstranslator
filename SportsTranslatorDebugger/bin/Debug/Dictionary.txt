Ole Einar Bj�rndalen:Ole Einar Bj�rndalen
10 km (f):10 km (fristil)
1000 m, final:1000 meter, finale
15 km (k/f), skiathlon:15 km (klassisk/fristil), skiathlon
1500 m, heat:1500 meter, heat
500 m, final:500 meter, finale
500 m, heat:500 meter, heat
500 m, semifinal:500 meter, semifinale
distans, 20 km:distanse, 20 km
fria programmet, final:friprogrammet, finale
Halfpipe, final:Halfpipe, finale
Lag, 5000 m, semifinal:Lag, 5000 meter, semifinale
Lag, gruppspel:lag, gruppespill
lilla backen, f�rsta hoppet:Liten bakke, f�rste omgang
Par, f�rsta �ket:par, f�rste omgang
<COUNTRY>Kuba</COUNTRY>:<COUNTRY>Cuba</COUNTRY>
<COUNTRY>Holland</COUNTRY>:<COUNTRY>Nederland</COUNTRY>
10000 m, final:10.000 meter, finale
1000 m, final:1000 meter, finale
1000 m, heat:1000 meter, heat
1000 m, kvartsfinal:1000 meter, kvartfinale
1000 m, semifinal:1000 meter, semifinale
15 km (f):15 km (fristil)
1500 m, final:1500 m, finale
1500 m, final:1500 meter, finale
1500 m, heat:1500 meter, heat
1500 m, semifinal:1500 meter, semifinale
30 km (k), masstart:30 km (klassisk), fellesstart
30 km (k/f), skiathlon:30 km (klassisk/finale), skiathlon
3 000 m, final:3000 meter, finale
50 km (k), masstart:50 km (klassisk), fellesstart
500 m, final:500 meter, finale
500 m, heat:500 meter, heat
500 m, kvartsfinal:500 meter, kvartfinale
500 m, semifinal:500 meter, semifinale
5 000 m, final:5000 meter, finale
5000 m, final:5000 meter, finale
Rodel:Aking
andra �ket:andre omgang
Australien:Australia
Avslutningsceremoni:Avslutningsseremoni
Belgien:Belgia
Bosnien-Hercegovina:Bosnia-Hercegovina
Brons:Bronse
Bronsmatch:Bronsekamp
Bulgarien:Bulgaria
Kanada:Canada
distans, 15 km:distanse, 15 km
Ella Gj�mle:Ella Gj�mle
Masstart, 12,5 km:Fellesstart, 12,5 km
Masstart, 15 km:Fellesstart, 15 km
fj�rde �ket:fjerde omgang
Fria programmet:Friprogrammet
Freestyle:Fristil
f�rsta �ket:f�rste omgang
Georgien:Georgia
Grupp A:Gruppe A
Grupp B:Gruppe B
Grupp B:Gruppe B
Grupp C:Gruppe C
gruppspel:gruppespill
Guld:Gull
Halfpipe, kval:Halfpipe, kvalifisering
Halfpipe, semifinal:Halfpipe, semifinale
Grekland:Hellas
Herrar:Herrer
Backhoppning:Hopp
Hopp, final, andra hoppet:Hopp finale, andre omgang
Hopp, final, f�rsta hoppet:Hopp, finale, f�rste omgang
Hopp, kval:Hopp, kvalifisering
Vitryssland:Hviterussland
Isdans, fria dansen:Isdans, fril�p
Isdans, obligatoriska dansen:Isdans, obligatorisk dans
Isdans, orginaldansen:Isdans, orginaldans
Italien:Italia
Jaktstart, 10 km (f):Jaktstart, 10 km (fristil)
Jaktstart, 10 km:Jaktstart, 10km
Match om femte plats:Kamp om femteplass
Match om plac 5-8:Kamp om plassering 5-8
Match om sjunde plats:Kamp om sjuendeplass
Nordisk kombination:Kombinert
Shorttrack:Kortbane
Korta programmet:Kortprogrammet
Kroatien:Kroatia
puckelpist, final:kulekj�ring, finale
individuellt, puckelpist, kval:kulekj�ring, kvalifisering
Konst�kning:Kunstl�p
Kvartsfinal:Kvartfinale
Damer:Kvinner
Lag, 3000 m, final:Lag, 3000 meter, finale
Lag, Bronsmatch:Lag, Bronsekamp
Lag, final:Lag, finale
Lag, Fyrmanna, heat 1:Lag, firer, heat 1
Lag, Fyrmanna, heat 2:Lag, firer, heat 2
Lag, Fyrmanna, heat 3:Lag, firer, heat 3
Lag, Fyrmanna, heat 4:Lag, firer, heat 4
lag, f�rf�ljelselopp, A-final:Lag, forf�lgelsesl�p, A-finale
Lag, f�rf�ljelselopp, B-final:Lag, forf�lgelsesl�p, B-finale
Lag, f�rf�ljelselopp, C-final:Lag, forf�lgelsesl�p, C-finale
Lag, f�rf�ljelselopp, D-final:Lag, forf�lgelsesl�p, D-finale
Lag, f�rf�ljelselopp, kvartsfinal:Lag, forf�lgelsesl�p, kvartfinale
Lag, f�rf�ljelselopp, semifinal:Lag, forf�lgelsesl�p, semifinale
Lag, gruppspel:Lag, gruppespillet
Lag, lilla backen, final:lag, liten bakke, finale
Lag, Lilla backen, kval:Lag, liten bakke, kvalifisering
Lag, lilla backen:Lag, litenbakke
Lag, semifinal:Lag, semifinale
Lag, Semifinal 1:Lag, Semifinale 1
Lag, Semifinal 2:Lag, Semifinale 2
Lag, sprint (f), final:Lag, sprint (fristil), finale
Lag, sprint (f), semifinal 1:lag, sprint (fristil), semifinale 1
Lag, sprint (f), semifinal 2:lag, sprint (fristil), semifinale 2
Lag, Utslagsomg 1:Lag, utslagsomgang 1
Lag, Utslagsomg 2:Lag, utslagsomgang 2
Lag, Utslagsomg 3:Lag, utslagsomgang 3
Skidor:Langrenn
Lettland:Latvia
lilla backen:liten bakke
lilla backen, andra hoppet:Liten bakke, andre omgang
lilla backen, kval:Liten bakke, kvalifisering
Makedonien:Makedonia
Marit Bj�rgen:Marit Bj�rgen
Moldavien:Moldovia
Nya Zeeland:New Zealand
Nordkorea:Nord-Korea
Uppvisningsgala:Oppvisningsgala
Par, andra �ket:Par, andre omgang
Par, Tv�manna, heat 1:Par, toer, heat 1
Par, Tv�manna, heat 2:Par, toer, heat 2
Par, Tv�manna, heat 3:Par, toer, heat 3
Par, Tv�manna, heat 4:Par, toer, heat 4
Parallellstorslalom, final:Parallellstorslal�m, finale
Parallellstorslalom, kval:Parallellstorslal�m, kvalifisering
parallellstorslalom, kvartsfinal:Parallellstorslal�m, kvartfinale
Parallellstorslalom, semifinal:Parallellstorslal�m, semifinale
Parallellstorslalom, utslagsomg:Parallellstorslal�m, utslagsomgang
parallellstorslalom, �ttondelsfinal:Parallellstorslal�m, �ttedelsfinale
Par, fria programmet:Parl�p, friprogrammet
Par, Korta programmet:Parl�p, kortprogrammet
Rum�nien:Romania
Ryssland:Russland
semifinal:semifinale
Serbien-Montenegro:Serbia-Montenegro
Skicross, final:Skicross, finale
Skicross, kval:Skicross, kvalifisering
Skicross, kvartsfinal:Skicross, kvartfinale
Skicross, semifinal:Skicross, semifinale
Skicross, �ttondelsfinal:Skicross, �ttedelfinale
Skidskytte:Skiskyting
Skridsko:Sk�yter
Slalom, andra �ket:Slal�m, andre omgang
Slalom, f�rsta �ket:Slal�m, f�rste omgang
Slovakien:Slovakia
Slovenien:Slovenia
Snowboardcross, final:Snowboardcross, finale
Snowboardcross, kval:Snowboardcross, kvalifisering
Snowboardcross, kvartsfinal:Snowboardcross, kvartfinale
Snowboardcross, semifinal:Snowboardcross, semifinale
Snowboardcross, �ttondelsfinal:Snowboardcross, �ttendelsfinale
Individuellt, sprint (k), final:sprint (klassisk), finale
Sprint (k), kval:Sprint (klassisk), kvalifisering
sprint (k), kvartsfinal:sprint (klassisk), kvartfinale
sprint (k), semifinal:sprint (klassisk), semifinale
Sprint, 7,5 km:Sprint, 7,5 km
stafett, 4x10 km (k/f):stafett, 4x10 km (klassisk/fristil)
stafett, 4x5 km (k/f):stafett, 4x5 km (klassisk/fristil)
Stafett, 4x6 km:Stafett, 4x6 km
Stafett, 4x7,5 km:Stafett, 4x7,5 km
Stafett, jaktstart, 4x5 km:Stafett, jaktstart, 4x5 km
stora backen:storbakke
stora backen, final:storbakke, finale
Stora backen, kval:Storbakke, kvalifisering
Storbritannien:Storbritania
Storslalom, andra �ket:Storslal�m, andre omgang
storslalom, f�rsta �ket:storslal�m, f�rste omgang
Superkombination, andra �ket:Superkombinasjonen, andre omgang
Superkombination, f�rsta �ket:Superkombinasjonen, f�rste omgang
Schweiz:Sveits
Scweiz:Sveits
Silver:S�lv
Sydkorea:S�r-Korea
tredje �ket:tredje omgang
Tjeckien:Tsjekkia
Turkiet:Tyrkia
Ungern:Ungarn
st�rtlopp:utfor
St�rtlopp:Utfor
�sterrike:�sterrike
�ttondelsfinal:�ttedelsfinale
Lag, gruppspel:
Kazakstan:
Estland:
Uzbekistan: