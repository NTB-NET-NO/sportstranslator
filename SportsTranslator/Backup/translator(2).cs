using System;
using System.Text;
using System.IO;
using System.Collections;



namespace SportsTranslator
{
	/// <summary>
	/// Summary description for translator.
	/// </summary>
	public class translator
	{
		public Hashtable arrDictionary = new Hashtable();
		

		public translator()
		{
			/*
			 * When first run we shall check if some directories exists
			 * If they don't they shall be created. If they exists we shall check for the dic-file
			 * 
			 * FIX:
			 * I am not sure that this is the right place for the code below
			 * But I'll leave it here for now.
			 */

			// This should read the text-file - if it has changed - then do search and replace
			string DicFilename = "Dictionary.txt";
			SportsTranslator.io fileDir = new SportsTranslator.io();

			// We check if the directory exists or not
			if (!fileDir.getDirectory("Dictionary")) 
			{
				fileDir.createDirectory("Dictionary");
			}

			if (fileDir.FileExists(DicFilename) == false) 
			{
				// We create the file empty
				fileDir.writeFile(DicFilename ,"");
			}

			arrDictionary = fileDir.ReadFileToArray(DicFilename );

		}

		/// <summary>
		/// This method shall find the word and - if it is in the dictionary-file
		/// we shall replace the name with other parameters
		/// </summary>
		/// <param name="content">String containing the file we are replacing words in</param>
		public string translate(string content) 
		{
			
			foreach (DictionaryEntry Item in arrDictionary ) 
			{
				/**
				 * ListItem newListItem = new ListItem();
        newListItem.Text = Item.Value.ToString();
        newListItem.Value = Item.Key.ToString();
        myDropDownList.Items.Add(newListItem);
		*/
				content = content.Replace(Item.Key.ToString(), Item.Value.ToString());
				// Doing the replace
				
			}

			return content;
		}

	}
}
