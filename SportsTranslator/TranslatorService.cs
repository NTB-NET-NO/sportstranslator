using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO;
using System.Timers;
using ntb_FuncLib;

using log4net;


namespace SportsTranslator
{
	public class TranslatorService : System.ServiceProcess.ServiceBase
    {
        private IContainer components;

        // Separating some directories so that we have capitalized and not capitalized

        // This is the directories where files enters and leaves
        // These should be found in the config-file
        public static string dirInput;        // This where the files arrives
        public static string dirOutput;       // This is where we send the files
        public static string dirDictionary;   // This is where we have the dictionary
        public static string dirError;
        


        // The following directories are system necessary
        public static string dirLog;      // This is where you'll find the log-file
        public static string dirDone;     // This is where we put the files that has been translated
        public static string dirError;    // This is where we send files that are errorous
        private System.Windows.Forms.Timer timerTranslater;     // This is where you'll find the original files

        public string Sourcename = "SportsTranslator";

        

        // Initialize the timer class
        Timer timer = new Timer();
        // LogFile logFile = new LogFile();
        
        
        


		public TranslatorService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call

            
            
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] 
            { 
                new TranslatorService() 
            };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);


            

		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.timerTranslater = new System.Windows.Forms.Timer(this.components);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
        /// 
        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            string dirInput = Properties.Settings.Default.DirectoryInput;
            string dirOutput = Properties.Settings.Default.DirectoryOutput;
            string dirDictionary = Properties.Settings.Default.DirectoryDictionary;
            string dirLog = Properties.Settings.Default.DirectoryLog;
            string dirError = Properties.Settings.Default.DirectoryError;
            string dirDone = Properties.Settings.Default.DirectoryDone;

            

            try
            {
                
                // string Sourcename = this.ServiceName;


                if (!EventLog.SourceExists(Sourcename))
                {
                    EventLog.CreateEventSource(Sourcename, "SportsTranslator");
                }

                EventLog evt = new EventLog();
                // EventLog.Source = "SportsTranslator";
                evt.Source = Sourcename;

                evt.Log = "SportsTranslator";

                evt.WriteEntry("Tried to use this.ServiceName which returns: " + this.ServiceName, EventLogEntryType.Information);
                evt.WriteEntry("In OnStart in TranslatorService", EventLogEntryType.Information);
                evt.WriteEntry("Starting Translator class", EventLogEntryType.Information);

                evt.WriteEntry("Getting settings from config-file!", EventLogEntryType.Information);


                // Adding the files in an array that we can loop through later
                evt.WriteEntry("Saving to arraylist!", EventLogEntryType.Information);
                ArrayList dir = new ArrayList();
                dir.Add(dirInput);
                dir.Add(dirOutput);
                dir.Add(dirLog);
                dir.Add(dirDictionary);
                dir.Add(dirDone);
                dir.Add(dirError);


                // Checking if directories exists and if they don't we create them
                evt.WriteEntry("Starting creating or checking if directories exists!", EventLogEntryType.Information);
                DirectoryInfo dirInfo = null;

                string Message = "";
                // Looping through the directories array
                foreach (String Dirs in dir)
                {
                    // if the directory does not exists, we shall create it
                    Message = "Created directory: " + Dirs;
                    if (!Directory.Exists(Dirs))
                    {
                        if (Dirs != null)
                        {
                            dirInfo = new DirectoryInfo(Dirs);
                            dirInfo.Create();
                        }
                        else
                        {
                            evt.WriteEntry("String was empty!",
                                EventLogEntryType.Warning);
                        }

                        // Write a message to the logfile
                        evt.WriteEntry(Message, EventLogEntryType.Information);


                    }
                    else
                    {

                        if (!Directory.Exists(dirLog))
                        {
                            evt.WriteEntry("Directory " + Dirs.ToString() + " exists. Not created!", EventLogEntryType.Information);
                        }
                        else
                        {
                            evt.WriteEntry("Using the logFile object to write to logfile",
                            EventLogEntryType.Information);
                            LogFile.MakePath(dirLog);
                            LogFile.WriteLog(ref dirLog, ref Message);
                        }

                    }

                }


                // Code below is marked out because we don't have to start it here.
                // We will start it in the timer
                /*
                EventLog.WriteEntry("Creating the Translator object", EventLogEntryType.Information);
                translator trans = new translator();

                EventLog.WriteEntry("Created translator. Now starting Translator object", EventLogEntryType.Information);
                trans.Start();
                 */

                // Getting the timer interval from the settings file
                string message = "Enabling timer";
                if (Directory.Exists(dirLog))
                {
                    LogFile.WriteLog(ref dirLog, ref message);
                }
                else
                {
                    evt.WriteEntry(message, EventLogEntryType.Information);
                }

                evt.WriteEntry("Starting the timer - a system timer",
                    EventLogEntryType.Information);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

                timer.Interval = 60000;

                //ad 3: enabling the timer
                timer.Enabled = true;

                try
                {
                    translator trans = new translator();
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(dirLog))
                    {
                        evt.WriteEntry(ex.ToString(),
                            EventLogEntryType.Error);

                        if (ex.InnerException.ToString() != "")
                        {
                            evt.WriteEntry(ex.InnerException.ToString(),
                                EventLogEntryType.Error);
                        }

                    }
                    else
                    {
                        string exeption = ex.ToString();
                        LogFile.WriteLog(ref dirLog, ref exeption);

                        if (ex.InnerException.ToString() != "")
                        {
                            string ie = ex.InnerException.ToString();
                            LogFile.WriteLog(ref dirLog, ref ie);
                        }
                    }
                }



            }
            catch (IOException iex)
            {
                EventLog evt = new EventLog();
                evt.Source = Sourcename;
                evt.Log = "SportsTranslator";
                evt.WriteEntry(iex.ToString(),
                    EventLogEntryType.Error);
            }

            catch (Exception ex)
            {
                EventLog evt = new EventLog();
                evt.Source = Sourcename;
                evt.Log = "SportsTranslator";
                evt.WriteEntry(ex.ToString(),
                    EventLogEntryType.Error);
            }
            

        }

		// protected override void _OnStart(string[] args)
        protected void _OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			// This should start the translator, read the translator-txt-file.

            // Creating directories
            // Populate the strings 
            string dirInput = Properties.Settings.Default.DirectoryInput;
            string dirOutput = Properties.Settings.Default.DirectoryOutput;
            string dirDictionary = Properties.Settings.Default.DirectoryDictionary;
            string dirLog = Properties.Settings.Default.DirectoryLog;
            string dirError = Properties.Settings.Default.DirectoryError;
            string dirDone = Properties.Settings.Default.DirectoryDone;

            try
            {
                string Sourcename = "SportsTranslator";
                // string Sourcename = this.ServiceName;

                if (!EventLog.SourceExists(Sourcename))
                {
                    EventLog.CreateEventSource(Sourcename, "SportsTranslator");
                }

                // EventLog.Source = "SportsTranslator";
                EventLog.Source = Sourcename;

                EventLog evt = new EventLog();
                // evt.Log = "Application";
                evt.Log = "SportsTranslator";

                evt.WriteEntry("Tried to use this.ServiceName which returns: " + this.ServiceName, EventLogEntryType.Information);
                evt.WriteEntry("In OnStart in TranslatorService", EventLogEntryType.Information);
                evt.WriteEntry("Starting Translator class", EventLogEntryType.Information);

                evt.WriteEntry("Getting settings from config-file!", EventLogEntryType.Information);


                // Adding the files in an array that we can loop through later
                evt.WriteEntry("Saving to arraylist!", EventLogEntryType.Information);
                ArrayList dir = new ArrayList();
                dir.Add(dirInput);
                dir.Add(dirOutput);
                dir.Add(dirLog);
                dir.Add(dirDictionary);
                dir.Add(dirDone);
                dir.Add(dirError);


                // Checking if directories exists and if they don't we create them
                evt.WriteEntry("Starting creating or checking if directories exists!", EventLogEntryType.Information);
                DirectoryInfo dirInfo = null;

                string Message = "";
                // Looping through the directories array
                foreach (String Dirs in dir)
                {
                    // if the directory does not exists, we shall create it
                    Message = "Created directory: " + Dirs;
                    if (!Directory.Exists(Dirs))
                    {
                        if (Dirs != null)
                        {
                            dirInfo = new DirectoryInfo(Dirs);
                            dirInfo.Create();
                        }
                        else
                        {
                            evt.WriteEntry("String was empty!",
                                EventLogEntryType.Warning);
                        }

                        // Write a message to the logfile
                        evt.WriteEntry(Message, EventLogEntryType.Information);


                    }
                    else
                    {

                        if (!Directory.Exists(dirLog))
                        {
                            evt.WriteEntry("Directory " + Dirs.ToString() + " exists. Not created!", EventLogEntryType.Information);
                        }
                        else
                        {
                            evt.WriteEntry("Using the logFile object to write to logfile",
                            EventLogEntryType.Information);
                            LogFile.MakePath(dirLog);
                            LogFile.WriteLog(ref dirLog, ref Message);
                        }

                    }

                }


                // Code below is marked out because we don't have to start it here.
                // We will start it in the timer
                /*
                EventLog.WriteEntry("Creating the Translator object", EventLogEntryType.Information);
                translator trans = new translator();

                EventLog.WriteEntry("Created translator. Now starting Translator object", EventLogEntryType.Information);
                trans.Start();
                 */

                // Getting the timer interval from the settings file
                string message = "Enabling timer";
                if (Directory.Exists(dirLog))
                {
                    LogFile.WriteLog(ref dirLog, ref message);
                }
                else
                {
                    evt.WriteEntry(message, EventLogEntryType.Information);
                }

                EventLog.WriteEntry("Starting the timer - a system timer",
                    EventLogEntryType.Information);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

                timer.Interval = 60000;

                //ad 3: enabling the timer
                timer.Enabled = true;

                try
                {
                    translator trans = new translator();
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(dirLog))
                    {
                        evt.WriteEntry(ex.ToString(),
                            EventLogEntryType.Error);

                        if (ex.InnerException.ToString() != "")
                        {
                            evt.WriteEntry(ex.InnerException.ToString(),
                                EventLogEntryType.Error);
                        }

                    }
                    else
                    {
                        string exeption = ex.ToString();
                        LogFile.WriteLog(ref dirLog, ref exeption);

                        if (ex.InnerException.ToString() != "")
                        {
                            string ie = ex.InnerException.ToString();
                            LogFile.WriteLog(ref dirLog, ref ie);
                        }
                    }
                }



            }
            catch (IOException iex)
            {
                EventLog evt = new EventLog();
                evt.WriteEntry(iex.ToString(),
                    EventLogEntryType.Error);
            }

            catch (Exception ex)
            {
                EventLog evt = new EventLog();
                evt.WriteEntry(ex.ToString(),
                    EventLogEntryType.Error);
            }

            


			
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
            string message = "In OnStop in TranslatorService";
            EventLog evt = new EventLog();
            evt.Source = Sourcename;
            evt.Log = "SportsTranslator";
            if (!Directory.Exists(dirLog))
            {
                evt.WriteEntry(message, EventLogEntryType.Information);
            }
            else
            {
                LogFile.WriteLog(ref dirLog, ref message);
            }

            message = "Stopping the timer";
            if (!Directory.Exists(dirLog))
            {
                evt.WriteEntry(message, EventLogEntryType.Information);

            }
            else
            {
                LogFile.WriteLog(ref dirLog, ref message);
            }

            timer.Enabled = false;
            
		}

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            translator trans = new translator();

            try
            {
                EventLog evt = new EventLog();
                evt.Log = "SportsTranslator";
                evt.Source = Sourcename;
                string message = "In OnElapsedTime in TranslatorService";
                if (!Directory.Exists(dirLog))
                {
                    evt.WriteEntry(message , EventLogEntryType.Information);
                } else {
                    LogFile.WriteLog(ref dirLog, ref message);
                }
                message = "Running TranslateFile";

                if (!Directory.Exists(dirLog))
                {
                    evt.WriteEntry(message, EventLogEntryType.Information);
                }
                else
                {
                    LogFile.WriteLog(ref dirLog, ref message);
                }
                // We are running the translator and translating files
                trans.TranslateFiles();
            }
            catch (Exception ex)
            {
                EventLog evt = new EventLog();
                evt.Source = Sourcename;
                evt.Log = Sourcename;
                EventLog.WriteEntry("Exception: " + ex.ToString(),
                    EventLogEntryType.Error);
            }
        }






	}
}
